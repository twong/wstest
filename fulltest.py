from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions



driver = webdriver.Firefox()


#driver.implicitly_wait(5) # Wait at least 5 seconds before calling it quits looking for a particular DOM element

driver.get('http://www.williams-sonoma.com/')

assert "Williams-Sonoma" in driver.title


#Problem 1: Close the overlay
#TODO: Should find a way to ensure that we can do this programmatically later.

WebDriverWait(driver,5).until(
    expected_conditions.element_to_be_clickable((By.CLASS_NAME,'overlayCloseX'))
    ).click()

#Click the Cookware button
mouseover= ActionChains(driver)
'''WebDriverWait(driver,5).until(
    expected_conditions.element_to_be_clickable((By.CLASS_NAME,'topnav-cookware '))
    ).click()'''
cookwareElement=driver.find_element_by_css_selector('a.topnav-cookware')
#mouseover.move_to_element(cookwareElement).click().perform()
#driver.execute_script('arguments[0].click();',cookwareElement)
driver.execute_script("arguments[0].fireEvent('onmouseover');",cookwareElement)
teaKettlesElement=driver.find_element_by_link_text('Tea Kettles')
driver.execute_script('arguments[0].click();',teaKettlesElement)
#mouseoverActionToFindCookwareAndClickOnTheThing.moveToElement(element).moveToElement(driver.find_element_by_css_selector(".topnav-cookware "))


